'use strict';

(function() {

  angular.module('bip-bip', []).factory('$bipBip', bipBipService);

  bipBipService.$inject = ['$window', '$rootScope', '$timeout'];


  // ---------------------------------------------------------- //
  // ---------------------------------------------------------- //


  function bipBipService($window, $rootScope, $timeout) {

    return {
      initPushNotification    : initPushNotification,
      onNotification          : onNotification,
      subscribeNotification   : subscribeNotification,
      unSubscribeNotification : unSubscribeNotification,
      setDebug                : setDebug
    };

    // ----------------------------- //


    function initPushNotification(appID, senderID, config) {

      if (angular.isDefined(config) && angular.isUndefined(config.customAction)) {
        config.customAction = onNotification;
      }

      $window.plugins.pushNotificationLoginEnabler.initPushNotification(appID, senderID, config);
    }


    function onNotification(notification) {
      $timeout(function() {
        $rootScope.$broadcast('$bipBip:notificationReceived', notification);
      });
    }


    function subscribeNotification(id) {
      $window.plugins.pushNotificationLoginEnabler.setPushIds(id);
    }


    function unSubscribeNotification() {
      $window.plugins.pushNotificationLoginEnabler.unSubscribeNotification();
    }


    function setDebug(debugStatus) {
      $window.plugins.pushNotificationLoginEnabler.setDebug(debugStatus);
    }

  }

})();
