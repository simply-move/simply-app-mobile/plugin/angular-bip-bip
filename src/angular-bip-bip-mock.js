'use strict';

(function() {

  angular.module('bip-bipMocks', []).factory('$bipBip', bipBipService);

  bipBipService.$inject = ['$rootScope', '$timeout'];


  // ---------------------------------------------------------- //
  // ---------------------------------------------------------- //


  function bipBipService($rootScope, $timeout) {

    return {
      initPushNotification    : initPushNotification,
      onNotification          : onNotification,
      subscribeNotification   : subscribeNotification,
      unSubscribeNotification : unSubscribeNotification,
      setDebug                : setDebug
    };

    // ----------------------------- //

    function initPushNotification(appID, senderID, config) {
      return { appID: appID, senderID: senderID, config: config };
    }

    function subscribeNotification(login) {
      return login;
    }

    function unSubscribeNotification() {
      return null;
    }

    function onNotification(notification) {
      $timeout(function() {
        $rootScope.$broadcast('$bipBip:notificationReceived', notification);
      });
    }

    function setDebug() {
      return true;
    }

  }

})();
