'use strict';

// ---------------------------------------------------------- //
// Dependencies
// ---------------------------------------------------------- //
var pkg     = require('./package.json');
var gulp    = require('gulp');

var uglify  = require('gulp-uglify');
var rename  = require('gulp-rename');
var header  = require('gulp-header');

var jshint  = require('gulp-jshint');
var jscs    = require('gulp-jscs');
var buddyjs = require('gulp-buddy.js');



// ---------------------------------------------------------- //
// Config
// ---------------------------------------------------------- //
var paths = {
  bowerFile : './bower.json',
  npmFile   : './package.json',
  gulpFile  : './gulpfile.js',
  config  : {
    jshint : './config/.jshintrc',
    jscs   : './config/.jscsrc'
  }
};

var filesToCheck = ['src/**.js', 'gulpfile.js'];


// ---------------------------------------------------------- //
// Main tasks
// ---------------------------------------------------------- //
gulp.task('build', ['uglify']);
gulp.task('lint', ['jshint', 'jscs', 'buddyjs']);


// ---------------------------------------------------------- //
// Checks code tasks - Linter, check style
// ---------------------------------------------------------- //
gulp.task('uglify', function() {

  var banner = ['/**',
    ' * <%= pkg.name %> - <%= pkg.description %>',
    ' * @version v<%= pkg.version %>',
    ' * @author <%= pkg.authors %>',
    ' * @link <%= pkg.repository.url %>',
    ' */',
    ''].join('\n');

  return gulp.src('src/**.js')
    .pipe(uglify())
    .pipe(header(banner, { pkg: pkg }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('dist'));
});


// ---------------------------------------------------------- //
// Checks code tasks - Linter, check style
// ---------------------------------------------------------- //
gulp.task('jshint', function() {
  return gulp.src(filesToCheck)
    .pipe(jshint(paths.config.jshint))
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('jscs', function() {
  return gulp.src(filesToCheck)
    .pipe(jscs(paths.config.jscs));
});

gulp.task('buddyjs', function() {
  return gulp.src(filesToCheck)
    .pipe(buddyjs({
      reporter : 'simple',
      ignore   : [0, 1]
    }));
});
