# Angular bip-bip

bip-bip gives you simple [AngularJS](https://angularjs.org/) wrapper for the bip-bip Cordova plugin (push notification).


Install manually, or form bower
```bash
$ bower install [url-of-the-git-repository]
```


## Test

To improve the code maintainability always run the lint command commit. Tools use by lint [JSHint](http://jshint.com/), [JSCS](http://jscs.info/), [BuddyJS](https://github.com/danielstjules/buddy.js/tree/master).
```bash
$ npm run lint
```



## Git Commit Guidelines

This project respect very precise rules over how git commit messages can be formatted. 

### Commit Message Format
Each commit message consists of a **header**, a **body** and a **footer**.  The header has a special
format that includes a **type**, a **scope** and a **subject**:

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

Example commit messages
```
git commit -m "fix(ProfessionalAccountController): display the Iban of the professional"
git commit -m "refactor: Changed other things"
```

Any line of the commit message cannot be longer 100 characters! This allows the message to be easier to read.

### Type
Must be one of the following:

* **feat**: A new feature
* **fix**: A bug fix
* **docs**: Documentation only changes
* **style**: Changes that do not affect the meaning of the code (white-space, missing semi-colons, etc...)
* **refactor**: A code change that neither fixes a bug or adds a feature
* **perf**: A code change that improves performance
* **test**: Adding missing tests
* **chore**: Changes to the build process or auxiliary tools and libraries such as documentation generation

### Scope
The scope could be anything specifying place of the commit change. For example `AppController`, `UserService`, `AboutTemplate`, etc...

### Subject
The subject contains succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize first letter
* no dot (.) at the end

### Body
Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes"
The body should include the motivation for the change and contrast this with previous behavior.

### Footer
The footer should contain any information about **Breaking Changes** and is also the place to
reference issues that this commit **Closes**.



## Versioning

This project is maintained under [the Semantic Versioning guidelines](http://semver.org/).
